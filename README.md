# GitLab-CI Integration for Kolla

## Add Component
When adding a new component to the build process, follow these steps:

1. Edit `releases.yml`, add component's Kolla-compatible alias (usable with kolla-build as a target).
2. Run `generate_gitlab_ci_yml.rb` to regenerate `.gitlab-ci.yml`.
3. Commit & push changes to run the pipeline.

## Override Component Template
If you need to add an override (change something in Kolla's j2 template for Dockerfile of the given component), follow
these steps:

1. Create `$COMPONENT_KOLLA_ALIAS.j2` in `template-overrides/$RELEASE` directory with the desired content.
2. Run `generate_gitlab_ci_yml.rb` to regenerate `.gitlab-ci.yml`.
3. Commit & push changes to run the pipeline.

`Notice:` Only named blocks in existing templates support the override mechanism! Content outside of
`{% block NAME %} ... {% endblock %}` CANNOT be overridden.

`Notice:` Overrides for common images (dependencies shared between components) MUST be added to override files of all
dependent components! Overrides for base images should be placed in `default.j2`.

For details on overriding templates, see official Kolla documentation. Or have a look at existing files in `template-overrides`.

## Trigger Build
Build of all releases is triggered by pushing changes to `master`.

## Keystone override
Find out parent id
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L53
   - Will find out parent id for parent id creation for autocreated projects (federated users have autocreated projects)
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/schema.py.patch
   - Schema update for enabling parent specification
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/utils.py.patch#L7
   - Utils update to support parents
  

Project mapping update
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L80
   - Find out current mapping (which projects does the user has access to)
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L87
   - Mapping to be deleted - `[current_assignment] - [mapping_which_should_be_present]`
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L110
   - Find all roles in a project to remove mapping from user (roles of user on 
     a specific project), delete all of them,
     - This is the problematic part, why the upstream may not include this ever.
       The lookup process takes too long (its cost efectivness was topic before,
       dont know whether anything has ever happend regarding this topic)
 

